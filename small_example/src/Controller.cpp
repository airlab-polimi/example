/**
 * Node Controller
 * File auto-generated on 28/05/2018 15:27:53
 */
#include "node_base/ROSNode.h"
#include "demo_icse/Controller_configuration.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "demo_icse/simple_controller.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"


class Controller : public node_base::ROSNode {
private:
	bool prepare();
	void tearDown();
	void errorHandling();
	void pose_cb_callback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
	void goal_cb_callback(const geometry_msgs::PoseStamped::ConstPtr& msg);
	void cmd_pub_callback(const ros::TimerEvent& );
	InternalState is;
	ros::Subscriber sub_pose_cb;
	ros::Subscriber sub_goal_cb;
	ros::Publisher pub_cmd_pub;
	ros::Timer timer_cmd_pub;
public:
	 Controller();
};

/**
 * Method nodeSigintHandler auto-generated
 */
void nodeSigintHandler(int sig) {
	g_request_shutdown = 1;
}

/**
 * Method main auto-generated
 */
int main(int argc, char** argv) {
	ros::init(argc, argv, "Controller", ros::init_options::NoSigintHandler);
	signal(SIGINT, nodeSigintHandler);
	Controller node;
	node.start();
	return 0;
}

/**
 * Method prepare auto-generated
 */
bool Controller::prepare() {
	Parameters p;
	handle.param<double>("k_values/p", p.k_values.p, 0.1);
	handle.param<double>("k_values/i", p.k_values.i, 0.5);
	handle.param<double>("k_values/d", p.k_values.d, 0.01);
	handle.param<double>("max_speed", p.max_speed, 3.0);
	handle.param<double>("min_speed", p.min_speed, 0.1);
	handle.param<double>("dt", p.dt, 0.1);
	is.initialize(&p);
	sub_pose_cb = handle.subscribe("/amcl_pose", 1, &Controller::pose_cb_callback, this);
	sub_goal_cb = handle.subscribe("/goal", 1, &Controller::goal_cb_callback, this);
	pub_cmd_pub = handle.advertise < geometry_msgs::Twist > ("/cmd_vel", 10);
	timer_cmd_pub = handle.createTimer(ros::Duration(0.05), &Controller::cmd_pub_callback, this);
	return true;
}

/**
 * Method tearDown auto-generated
 */
void Controller::tearDown() {
	ROS_INFO("Node is shutting down");
	return;
}

/**
 * Method errorHandling auto-generated
 */
void Controller::errorHandling() {
	ROSNode::errorHandling();
}

/**
 * Method pose_cb_callback auto-generated
 */
void Controller::pose_cb_callback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) {
	savePose(is.vars(), is.params(), msg);
}

/**
 * Method goal_cb_callback auto-generated
 */
void Controller::goal_cb_callback(const geometry_msgs::PoseStamped::ConstPtr& msg) {
	getGoal(is.vars(), is.params(), msg);
}

/**
 * Method cmd_pub_callback auto-generated
 */
void Controller::cmd_pub_callback(const ros::TimerEvent& ) {
	pub_cmd_pub.publish(sendCmd(is.vars(), is.params()));
}

/**
 * Method Controller auto-generated
 */
 Controller::Controller() {
	setName(ros::this_node::getName());
}

