#include "demo_icse/state_mux.h"

void updateState(Variables_ptr v, const std_msgs::Int32::ConstPtr& msg) {
    v->current_state = msg->data;
}

void saveCmd(Variables_ptr v, int position, const geometry_msgs::Twist::ConstPtr& msg) {
    v->input_list[position] = geometry_msgs::Twist(*msg.get());
}

geometry_msgs::Twist selectCmd(Variables_ptr v) {
    geometry_msgs::Twist out;
    switch(v->current_state) {
        case 2:
        case 3:
            out = geometry_msgs::Twist(v->input_list[0]);
            break;
        case 4:
            out = geometry_msgs::Twist(v->input_list[1]);
            break;
    }
    return out;
}
