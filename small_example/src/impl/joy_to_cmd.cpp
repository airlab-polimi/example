#include "demo_icse/joy_to_cmd.h"

geometry_msgs::Twist joyToCmd(Variables_ptr v, Parameters_ptr p, const sensor_msgs::Joy::ConstPtr& msg) {
  if(msg->buttons[p->button_mapping.halt] == 1)
      v->next_state = 0;
  if(msg->buttons[p->button_mapping.safe] == 1)
      v->next_state = 1;
  if(msg->buttons[p->button_mapping.manual] == 1)
      v->next_state = 2;
  if(msg->buttons[p->button_mapping.assisted] == 1)
      v->next_state = 3;
  if(msg->buttons[p->button_mapping.autonomous] == 1)
      v->next_state = 4;
  
  geometry_msgs::Twist out;
  if(msg->buttons[p->button_mapping.enabler] == 1) {
      out.linear.x = msg->axes[p->axis_mapping.forward] * p->scale.linear;
      out.angular.z = msg->axes[p->axis_mapping.rotate] * p->scale.angular;
  }
  
  return out;
}

