/**
 * Node Out_Mux
 * File auto-generated on 28/05/2018 15:27:53
 */
#include "node_base/ROSNode.h"
#include "demo_icse/Out_Mux_configuration.h"
#include "std_msgs/Int32.h"
#include "demo_icse/state_mux.h"
#include "geometry_msgs/Twist.h"


class Out_Mux : public node_base::ROSNode {
private:
	bool prepare();
	void tearDown();
	void errorHandling();
	void state_cb_callback(const std_msgs::Int32::ConstPtr& msg);
	void input_1_cb_callback(const geometry_msgs::Twist::ConstPtr& msg);
	void input_2_cb_callback(const geometry_msgs::Twist::ConstPtr& msg);
	void output_pub_callback(const ros::TimerEvent& );
	InternalState is;
	ros::Subscriber sub_state_cb;
	ros::Subscriber sub_input_1_cb;
	ros::Subscriber sub_input_2_cb;
	ros::Publisher pub_output_pub;
	ros::Timer timer_output_pub;
public:
	 Out_Mux();
};

/**
 * Method nodeSigintHandler auto-generated
 */
void nodeSigintHandler(int sig) {
	g_request_shutdown = 1;
}

/**
 * Method main auto-generated
 */
int main(int argc, char** argv) {
	ros::init(argc, argv, "Out_Mux", ros::init_options::NoSigintHandler);
	signal(SIGINT, nodeSigintHandler);
	Out_Mux node;
	node.start();
	return 0;
}

/**
 * Method prepare auto-generated
 */
bool Out_Mux::prepare() {
	is.initialize();
	sub_state_cb = handle.subscribe("/global_state", 1, &Out_Mux::state_cb_callback, this);
	sub_input_1_cb = handle.subscribe("/input_1", 1, &Out_Mux::input_1_cb_callback, this);
	sub_input_2_cb = handle.subscribe("/input_2", 1, &Out_Mux::input_2_cb_callback, this);
	pub_output_pub = handle.advertise < geometry_msgs::Twist > ("/mobile_base/commands/velocity", 10);
	timer_output_pub = handle.createTimer(ros::Duration(0.05), &Out_Mux::output_pub_callback, this);
	return true;
}

/**
 * Method tearDown auto-generated
 */
void Out_Mux::tearDown() {
	ROS_INFO("Node is shutting down");
	return;
}

/**
 * Method errorHandling auto-generated
 */
void Out_Mux::errorHandling() {
	ROSNode::errorHandling();
}

/**
 * Method state_cb_callback auto-generated
 */
void Out_Mux::state_cb_callback(const std_msgs::Int32::ConstPtr& msg) {
	updateState(is.vars(), msg);
}

/**
 * Method input_1_cb_callback auto-generated
 */
void Out_Mux::input_1_cb_callback(const geometry_msgs::Twist::ConstPtr& msg) {
	saveCmd(is.vars(), 0, msg);
}

/**
 * Method input_2_cb_callback auto-generated
 */
void Out_Mux::input_2_cb_callback(const geometry_msgs::Twist::ConstPtr& msg) {
	saveCmd(is.vars(), 1, msg);
}

/**
 * Method output_pub_callback auto-generated
 */
void Out_Mux::output_pub_callback(const ros::TimerEvent& ) {
	pub_output_pub.publish(selectCmd(is.vars()));
}

/**
 * Method Out_Mux auto-generated
 */
 Out_Mux::Out_Mux() {
	setName(ros::this_node::getName());
}

