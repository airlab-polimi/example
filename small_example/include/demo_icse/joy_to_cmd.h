#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Joy.h"
#include "demo_icse/Joy_Control_configuration.h"

geometry_msgs::Twist joyToCmd(Variables_ptr v, Parameters_ptr p, const sensor_msgs::Joy::ConstPtr& msg);

