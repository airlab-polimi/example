#ifndef _CONTROLLER_CONFIGURATION_H
#define _CONTROLLER_CONFIGURATION_H
/**
 * Auto-generated Internal State
 */
#include "node_base/Configuration.h"

struct Variables: node_base::VariablesBase { 
	double goal[3];
	double pose[3];
	double speed;
	bool rotate;
	Variables () {
		speed = 0;
	};
};
struct Parameters: node_base::ParametersBase { 
	struct k_values_t { 
		double p;
		double i;
		double d;
	} k_values;
	double max_speed;
	double min_speed;
	double dt;
};
typedef std::shared_ptr < const Parameters > Parameters_ptr;
typedef std::shared_ptr < Variables > Variables_ptr;
class InternalState: node_base::InternalStateBase {
public:
	Variables_ptr vars() {
		return std::static_pointer_cast < Variables > (_vars);
	};

	Parameters_ptr params() const {
		return std::static_pointer_cast < const Parameters > (_params);
	};

	void initialize (node_base::ParametersBase * p = NULL) {
		_params = std::make_shared < const Parameters > (*static_cast < Parameters * > (p));
		_vars = std::make_shared < Variables > ();
	}
};
#endif