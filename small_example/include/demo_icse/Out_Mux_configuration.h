#ifndef _OUT_MUX_CONFIGURATION_H
#define _OUT_MUX_CONFIGURATION_H
/**
 * Auto-generated Internal State
 */
#include "node_base/Configuration.h"
#include "geometry_msgs/Twist.h"

struct Variables: node_base::VariablesBase { 
	geometry_msgs::Twist input_list[2];
	int current_state;
	Variables () {
		current_state = 0;
	};
};
typedef std::shared_ptr < Variables > Variables_ptr;
class InternalState: node_base::InternalStateBase {
public:
	Variables_ptr vars() {
		return std::static_pointer_cast < Variables > (_vars);
	};

	void initialize (node_base::ParametersBase * p = NULL) {
		_vars = std::make_shared < Variables > ();
	}
};
#endif