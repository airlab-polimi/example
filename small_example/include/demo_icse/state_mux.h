#include "std_msgs/Int32.h"
#include "geometry_msgs/Twist.h"
#include "demo_icse/Out_Mux_configuration.h"

void updateState(Variables_ptr v, const std_msgs::Int32::ConstPtr& msg);

void saveCmd(Variables_ptr v, int position, const geometry_msgs::Twist::ConstPtr& msg);

geometry_msgs::Twist selectCmd(Variables_ptr v);

