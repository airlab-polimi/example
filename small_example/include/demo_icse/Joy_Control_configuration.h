#ifndef _JOY_CONTROL_CONFIGURATION_H
#define _JOY_CONTROL_CONFIGURATION_H
/**
 * Auto-generated Internal State
 */
#include "node_base/Configuration.h"

struct Variables: node_base::VariablesBase { 
	int next_state;
	Variables () {
		next_state = 0;
	};
};
struct Parameters: node_base::ParametersBase { 
	struct button_mapping_t { 
		int halt;
		int safe;
		int manual;
		int assisted;
		int autonomous;
		int enabler;
	} button_mapping;
	struct axis_mapping_t { 
		int forward;
		int rotate;
	} axis_mapping;
	struct scale_t { 
		double linear;
		double angular;
	} scale;
};
typedef std::shared_ptr < const Parameters > Parameters_ptr;
typedef std::shared_ptr < Variables > Variables_ptr;
class InternalState: node_base::InternalStateBase {
public:
	Variables_ptr vars() {
		return std::static_pointer_cast < Variables > (_vars);
	};

	Parameters_ptr params() const {
		return std::static_pointer_cast < const Parameters > (_params);
	};

	void initialize (node_base::ParametersBase * p = NULL) {
		_params = std::make_shared < const Parameters > (*static_cast < Parameters * > (p));
		_vars = std::make_shared < Variables > ();
	}
};
#endif