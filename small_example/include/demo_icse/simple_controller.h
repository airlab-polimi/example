#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "demo_icse/Controller_configuration.h"

void savePose(Variables_ptr v, Parameters_ptr p, const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);

void getGoal(Variables_ptr v, Parameters_ptr p, const geometry_msgs::PoseStamped::ConstPtr& msg);

geometry_msgs::Twist sendCmd(Variables_ptr v, Parameters_ptr p);

